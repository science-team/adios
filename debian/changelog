adios (1.13.1-43) UNRELEASED; urgency=medium

  * Drop kfreebsd support
  * Drop ia64 references in d/control; no  longer suppported

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 16 Dec 2024 12:10:44 +0000

adios (1.13.1-42) unstable; urgency=medium

  * Build for python3.12 only. Fails to compile for python3.13, and
    adios being deprecated in favour of adios2. Closes: #1087684

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 21 Nov 2024 16:28:21 +0000

adios (1.13.1-41) unstable; urgency=medium

  * bp2bp.openmpi is .mpich on 32-bit systems. Closes: #1087527
  * Undo hard-coded python3.12 now that numpy rebuilt
  * libhdf5-*-dev also need to depend on correct mpi implementation

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 15 Nov 2024 14:43:20 +0000

adios (1.13.1-40) unstable; urgency=medium

  * Simplify and standardise WITH_IBVERRBS
  * Use chrpath to remove unnecessary runpaths
  * Fix buildpaths to fix FTBFS. Closes: #1081052

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 14 Nov 2024 11:05:37 +0000

adios (1.13.1-39) unstable; urgency=medium

  * python3.12 patch: label strings used for regexps. Closes: #1081052

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 29 Sep 2024 06:48:48 +0100

adios (1.13.1-38) unstable; urgency=medium

  [ Alastair McKinstry ]
  * Also update control.in S-V
  * Move from pkg-config to pkgconf
  * patch to drop dash-seperated names in setup.cfg
  * Update py3.patch - drop find_executable(), deprecated

  [ Gianfranco Costamagna ]
  * Correct the NMU content
  * Replace some more pkg-config into pkgconf

  [ zhangdandan <zhangdandan@loongson.cn> ]
  * add build support for loongarch64 (Closes: #1058668)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 27 Sep 2024 15:58:15 +0200

adios (1.13.1-37) unstable; urgency=medium

  * Fix FTBFS with gcc14 compiler. Closes: #1074801
  * Standards-Version: 4.7.0; no change required
  * Set Debian Science Maint. as maintainer

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 12 Aug 2024 19:26:59 +0100

adios (1.13.1-36) unstable; urgency=medium

  * Only build with openmpi on 64-bit archs

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 03 Apr 2024 09:36:55 +0100

adios (1.13.1-35) unstable; urgency=medium

  * Patch to fix FTBFS with -Werror=implicit-function-declaration.
    Closes: #1066553
  * Fix typos in VCS URLS in d/control

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 14 Mar 2024 14:18:47 +0000

adios (1.13.1-34) unstable; urgency=medium

  * B-D on dh-fortran-mod (>= 0.32)
  * Revert FMODDIR from /usr/include to libdir via dh-fortran
  * Update d/control to use fortran:Depends, fortran:Recommends

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 03 Jan 2024 08:57:09 +0000

adios (1.13.1-33) unstable; urgency=medium

  * Use cython3-legacy. Closes: #1056786

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 09 Dec 2023 18:36:44 +0000

adios (1.13.1-32) unstable; urgency=medium

  * Update paths for setuptools changes. Closes: #1054815
  * Standards-Version: 4.6.2

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 04 Nov 2023 10:38:35 +0000

adios (1.13.1-31) unstable; urgency=medium

   * ABITAG in update-alternatives not set correctly when multiple python3
     versions offered. Closes: #1025923
  * Standards-Version: 4.6.1

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 12 Dec 2022 10:56:54 +0000

adios (1.13.1-30) unstable; urgency=medium

  * Ensure libadios-*-dev provide libadios-mpi-dev
  * Fix autopkgtest to work with libadios-openmpi-dev | libadios-mpi-dev
    Closes: #998296

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 02 Nov 2021 11:49:42 +0000

adios (1.13.1-29) unstable; urgency=medium

  * Standards-Version: 4.6.0
  * Build sequential adios.pc. (LP: #1913205) 
  * Add initial autopkg tests

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 06 Sep 2021 17:28:33 +0100

adios (1.13.1-28) unstable; urgency=medium

  * postinst/prerm: Ensure generated tags correct.
    Also cope with situation where supported python changes between
    install/removal. Closes: #983888
  * Change condition to support gfortran >=9, not just <=10. Closes: #983962

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 06 Mar 2021 16:35:45 +0000

adios (1.13.1-27) unstable; urgency=medium

  * Fix build with cython3. Closes: #972420
  * Change default branch to debian/latest in d/control
  * Add 'set -e' to trap shell errors in debian/rules
  * Standards-Version: 4.5.1

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 28 Jan 2021 13:27:55 +0000

adios (1.13.1-26) unstable; urgency=medium

  * Further regeession in postinst scripts fixed. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 26 Sep 2020 11:57:59 +0100

adios (1.13.1-25) unstable; urgency=medium

  * Change branch paths in d/gpb.conf to DEP-14 and repos
  * Fix regression in autogenerating postinst scripts

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 03 Sep 2020 14:40:47 +0100

adios (1.13.1-24) unstable; urgency=medium

  * d/not-installed: use wildcard instead of arch for paths
  * Fix incomplete move to subst vars

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 06 Aug 2020 08:43:40 +0100

adios (1.13.1-23) unstable; urgency=medium

  * Fix FTBFS with gfortran 10, Closes: #966966
  * Use debhelper 13
  * Use subst vars instead of autogenerating dh files
  * Add debian/not-installed

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 03 Aug 2020 18:39:34 +0100

adios (1.13.1-22) unstable; urgency=medium

  * Change update-alternatives code in postinst to dynamically detect
    python3 version. Closes: #956864

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 11 Apr 2020 19:43:38 +0100

adios (1.13.1-21) unstable; urgency=medium

  * Rebuild with netcdf-mpi-15

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 13 Mar 2020 10:16:06 +0000

adios (1.13.1-20) unstable; urgency=medium

  * Drop Fortran-Mod: clause no longer used in dh_fortran_mod
  * Use dh-sequence-* instead of dh --with *
  * Drop references to python-breaks/etc before stable
  * Standards-Version: 4.5.0
  * Patch needed (netcdf-order.patch) for netcdf-parallel transition
  * Drop obsolete breaks/depends in d/control.in
  
 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 25 Feb 2020 12:06:17 +0000

adios (1.13.1-19) unstable; urgency=medium

  * Fix: re-include regression: dropped link in d/rules. Closes: #933796 
  * Update pre/post scripts; drop obsolete refs to py36

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 09 Aug 2019 11:50:53 +0100

adios (1.13.1-18) unstable; urgency=medium

  * Rebuild against latest mpi4py
  * Drop references to cython  ; ensure python3 not python is used.
  * B-D on python3-numpy

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 01 Aug 2019 09:50:21 +0100

adios (1.13.1-17) unstable; urgency=medium

  * B-D on gfortran | fortran-compiler
  * Drop python2 support
  * Now use debhelper-compat = 12
  * Standards-Version: 4.4.0

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 19 Jul 2019 09:23:03 +0100

adios (1.13.1-16) unstable; urgency=medium

  * Check full path of link for /etc/alternatives in python3-adios.postinst
    and delete obsolete link if present. Closes: #920700

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 29 Jan 2019 09:23:59 +0000

adios (1.13.1-15) unstable; urgency=medium

  * Patch from Steve Langasek for unaligned accesses that cause armhf
    binaries to crash on arm64. Closes: #919763

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 28 Jan 2019 09:39:26 +0000

adios (1.13.1-14) unstable; urgency=medium

  * Don't run tests on armhf. When built on arm64, there are known failures
    due to unaligned accesses. Closes: #919763

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 25 Jan 2019 06:23:43 +0000

adios (1.13.1-13) unstable; urgency=medium

  * Fix for python3-adios postinst deleting old py36 file. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 06 Jan 2019 07:28:16 +0000

adios (1.13.1-12) unstable; urgency=medium

  * Standards-Version: 4.3.0
  * Set dev packages as M-A: same
  * Move Fortran mod files to $fmoddir
  * Don't ship dup mod files
  * Remove hard-coded python3.6 alternatives

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 01 Jan 2019 20:26:35 +0000

adios (1.13.1-11) unstable; urgency=medium

  * use dh_auto_build/dh_auto_install for the C parts. Patch thanks to Pino
    Toscano. Closes: #914581

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 26 Nov 2018 09:45:08 +0000

adios (1.13.1-10) unstable; urgency=medium

  * Add parallel-netcdf support
   - Add patch needed for mssing-header io_timer.h
  * Fix typo in d/copyright

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 08 Nov 2018 13:13:32 +0000

adios (1.13.1-9) unstable; urgency=medium

  * Fix broken symlink libadios*_internal_nompi.f. Closes: #911048

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 30 Oct 2018 11:26:36 +0000

adios (1.13.1-8) unstable; urgency=medium

  * Update copyright information. Closes: #909799
  * Push multi-mpi support to unstable. Closes: #900804.
  * Drop python3-adios dep. on python-adios
  * Move *.mod files into M-A paths. Closes: #909862, #874233.
  * Fix update-alternatives. Closes: #910205, #910199.
  * Drop hard-coded xz compression
  * Drop old Breaks/Replaces, no longer needed for Buster
  * Drop obsolete patches:
    - errno.patch
    - gcc6-fixes.patch
    - multiarch_safe.patch
    - nompi_flag.patch

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 09 Oct 2018 20:44:24 +0100

adios (1.13.1-7) experimental; urgency=medium

  * Re-work to provide multi-mpi support: now provide
    lib-$MPI-dev packages

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Sep 2018 07:04:54 +0100

dios (1.13.1-6) unstable; urgency=medium

  * Be more aggressive in .libs deletion
  * Typo in rules : LZ4 is in /usr

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 24 Sep 2018 15:25:46 +0100

adios (1.13.1-5) unstable; urgency=medium

  * Standards-Version: 4.2.1
  * Add LZ4 support
  * Add BLOSC support
  * Remove obsolete pyshared references
  * Drop M-A: same on python packages as they ship compiled code
  * Drop use of dpkg-parsechangelog
  * Don't ship .libs dirs
  * Add fortran_mod support

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 16 Sep 2018 10:48:06 +0100

adios (1.13.1-4) unstable; urgency=medium

  * Build with python3.7 support

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 28 Jun 2018 09:05:36 +0100

adios (1.13.1-3) unstable; urgency=medium

  * Fix FTBFS with dpkg-buildpackage -A. Closes: #901103

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 11 Jun 2018 15:08:54 +0100

adios (1.13.1-2) unstable; urgency=medium

  * Drop obsolete X-Python-Version statements
  * Fix typo in adios_config that breaks operation. Closes: #900811.
  * Add pkg-config as dependency to libadios-dev
  * Set VERSIONSTRING in adios_config. Closes: #894165

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 05 Jun 2018 16:39:47 +0100

adios (1.13.1-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 04 May 2018 07:26:40 +0100

adios (1.13.0-3) unstable; urgency=medium

  * Rebuild against openmpi3
  * Change ibverbs -> infiniband
  * MPI_LIBS now extracted from pkg-config for MPI, not hard-coded per arch
  * Standards-Version: 4.1.4

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 18 Apr 2018 09:10:20 +0100

adios (1.13.0-2) unstable; urgency=medium

  * Standards-Version: 4.1.3; no changes required
  * Move VCS to salsa.debian.org
  * Ack. bugs fixed in previous release: Closes: #789464, 3874233

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 01 Mar 2018 16:47:47 +0000

adios (1.13.0-1) unstable; urgency=medium

  * New upstream release
  * Mark libadios-examples as M-A: foreign
  * Standards-Version: 4.1.2; no changes required
  * Change Vcs reference in control to https for security
  * Add pointer to LGPL in common-licenses
  * Set exec bit correctly on example scripts

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 12 Dec 2017 17:08:50 +0000

adios (1.12.0-4) unstable; urgency=medium

  * Standards-Version: 4.1.1
  * Drop dependency on libhdf5-serial-dev in favour of liubhdf5-dev
    Closes: #879132

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 20 Oct 2017 06:04:09 +0100

adios (1.12.0-3) unstable; urgency=medium

  * Move to Standards-Version: 4.1.0
  * Remove Multi-Arch: same from libadios-dev

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 29 Aug 2017 14:21:05 +0100

adios (1.12.0-2) experimental; urgency=medium

  * Fix FTBFS due to missing directory when examples not built.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 29 Aug 2017 13:55:41 +0100

adios (1.12.0-1) experimental; urgency=medium

  * New upstream release 
  * Add mxml license to debian/copyright 
  * Split examples into separate package (40+MB of Arch: all)
  * Add gdb.conf file
  * Typo in rules: make sure builds for all py3 versions
  * Drop redundant dh-autoreconf  build-depends
  * Fix executable perms typo
  * Standards-Vertsion: 4.0.1

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 14 Aug 2017 08:11:51 +0100

adios (1.11.0-2) unstable; urgency=medium

  * Fix support for multiple py3 versions. Closes: #867177
  * S-V: 4.0.0. No other changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 21 Apr 2017 10:25:50 +0100

adios (1.11.0-1) unstable; urgency=medium

  * New upstream release
  * Obsolete patches: 
    - typedef-bool-already-defined.patch
    - version-fix.patch
  * Don't ship ad_config as bin but as python helper module

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 21 Nov 2016 13:11:27 +0000

adios (1.10.1-1) unstable; urgency=medium

  * New upstream release
    - sh4.patch obsolete
    - python3 now upstream in adios.
    - security.patch now merged upstream
  * DH_COMPAT=10
  * New patch type-bool-already-defined.patch to fix FTBFS against HDF5 1.10.
    Thanks to Giles Filippini, Closes: #841967

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 23 Oct 2016 01:13:26 +0100

adios (1.9.0-12) unstable; urgency=medium

  * Fix for gcc 6.2: Don't define 'bool', its provided.
  * Regresssion: adios_config -d now works. Closes: #834929.
  * Remove unnnecessary build-dependency on bash.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 25 Aug 2016 21:46:00 +0100

adios (1.9.0-11) unstable; urgency=medium

  * Don't use -lcr for hppa. Closes: #827574

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 20 Jun 2016 11:18:27 +0100

adios (1.9.0-10) unstable; urgency=medium

  * Standards-Version: 3.9.8; no changes required
  * Hard-code shell in debian/rules for reproducibility

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 17 Jun 2016 09:16:17 +0100

adios (1.9.0-9) unstable; urgency=medium

  * Add SZIP support.
  * Remove hppa, x32 specific rules for mpich. Default to openmpi
  * Fix for libc6 (2.23 in experimental). Closes: #818825

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 22 Mar 2016 11:52:49 +0000

adios (1.9.0-8) unstable; urgency=medium

  * Build s390x on openmpi default. Don't try to link against -lmpich.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 26 Feb 2016 06:03:01 +0000

adios (1.9.0-7) unstable; urgency=medium

  * Fix for openmpi1.10 transition. Closes: #813724.
  * Move to Standards-Version: 3.9.7
  * Add lintian overrides for false-alerts on tmpl scripts

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 03 Feb 2016 21:11:07 +0000

adios (1.9.0-6) unstable; urgency=medium

  * Don't include '-lcr' on x32, 68k either.
  * Use mpif90 instead of mpif77 for MPIFC. (Fix FTBFS on sparc)

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 01 Jan 2016 22:46:11 +0000

adios (1.9.0-5) unstable; urgency=medium

  * Fix typo in build rules for s390x.
  * Don't include '-lcr' for MPI builds on hppa

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 04 Nov 2015 18:27:29 +0000

adios (1.9.0-4) unstable; urgency=medium

  * Fix MPI_LIBS for s390x build

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 01 Nov 2015 06:41:15 +0000

adios (1.9.0-3) unstable; urgency=medium

  * Remove mpi-workaround.patch, breaks build on non-mpich archs

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 29 Oct 2015 17:02:01 +0000

adios (1.9.0-2) unstable; urgency=medium

  * Build against all mpi versions again.
  * Link adios_mpi.so python packages against mpi libs 
  * delete old binaries from examples

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 29 Oct 2015 01:32:35 +0000

adios (1.9.0-1) experimental; urgency=medium

  * New upstream release
  * Changes needed for python3 - adapt pyx scripts
  * Temporarily only build for mpich as bug in openmpi 
  * Re-enable tests on bigendian systems for checking
  * Add patch to fix version number.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 27 Oct 2015 11:52:01 +0000

adios (1.8.0-5) unstable; urgency=medium

  * Stay on netcdf-dev only in unstable before netcdf-fortran transition. 
    Closes: #790230.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 31 Jul 2015 02:59:21 +0100

adios (1.8.0-4~exp1) unstable; urgency=medium

  * Patch from Steve Langasek for python3.5 transition. Closes: #793449 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 24 Jul 2015 08:13:01 +0100

adios (1.8.0-3) experimental; urgency=medium

  * Build against libnetcdff-dev for netcdf-transition.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 13 Jul 2015 13:27:51 +0100

adios (1.8.0-2) unstable; urgency=medium

  * Ensure CFLAGS are included in build
  * Add Breaks/Replaces/Provides to handle move of /etc/skel.py 
    Closes: #789663.
  * Put Vcs-Git: reference in canonical anonscm.debian.org form 
  * Make examples multiarch safe by fixing @host@, @build@ in Makefiles
    to call dpkg-architecture at runtime. Closes: #789464

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 01 Jul 2015 17:07:14 +0100

adios (1.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Standards-Version: 3.9.6.	
  * Work with both parallel and serial HDF5.
  * Enable glib support
  * adios_config: Fix typo: pkgconfig -> pkg-config
  * No longer ship FindADIOS.cmake; recommend pkg-config instead.
    Closes: #784766.
  * Re-enable tests for big-endian systems on sid 
  * Add Vcs-git: pointing to debian-science repo. 
  * Move bash_completions to /usr/share/bash-completion/completions
  * Patch to fix stricter automake error checking

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 19 Jun 2015 15:08:17 +0100

adios (1.7.0-2) unstable; urgency=medium

  * PATH not inherited correctly. export it.
  * Cope with python3.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 17 Jun 2014 08:45:36 +0100

adios (1.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Add adios.pc pkgconfig file. adios_config now uses this.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 16 Jun 2014 23:06:38 +0100

adios (1.6.0-7) unstable; urgency=medium

  * Use python3-all-dev to build on all python3 versions.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 23 May 2014 03:02:48 +0100

adios (1.6.0-6) unstable; urgency=medium

  * Fix typo that broke build of python2.7 wrapper. Closes: #749009.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 22 May 2014 11:58:23 +0100

adios (1.6.0-5) unstable; urgency=medium

  * Don't depend on infiniband on hurd, kfreebsd-*
  * Build with serial HDF5 by default; building with mpicc.openmpi collides
    with mpipublic.h internally.
  * Prime CMakeCache.txt with PYTHON_MPI4PY_INCLUDE_DIR so it is found
    on all archs to fix FTBFS.
  * Add initial bash_completion.d completions.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 22 May 2014 07:02:04 +0100

adios (1.6.0-4) unstable; urgency=medium

  * Add python-adios, python3-adios wrapper packages.
  * Add infiniband, bzip2, libz as 'transports'

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 19 May 2014 15:05:17 +0100

adios (1.6.0-3) unstable; urgency=medium

  * Ensure makefile doesn't fail on shell conditional in debian/rules

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Mar 2014 21:36:36 +0000

adios (1.6.0-2) unstable; urgency=medium

  * Disable tests on big-endian platforms due to upstream test bug.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Mar 2014 18:38:22 +0000

adios (1.6.0-1) unstable; urgency=medium

  * New upstream. Closes: #732276, #727312.
  * Ship in xz format. 

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 18 Feb 2014 00:36:53 +0000

adios (1.5.0-1) unstable; urgency=medium

  * New upstream.
  * Standards-Version: 3.9.5
  * Include latest config.{sub,guess} 
  * New watch file.
  * Create libadios-bin for binaries.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 09 Dec 2013 15:21:31 +0000

adios (1.4.1-1) unstable; urgency=low

  * New upstream.
  * update config.{sub,guess} for the AArch64 port. Closes: #727312.
  * Make python code python2-3 agnostic.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 24 Oct 2013 19:27:47 +0100

adios (1.3-11) unstable; urgency=low

  * Comment out lustre support (and remove dependency) as Wheezy will not
    ship with lustre-dev. Closes: #699262.
  * Standards-Version: 3.9.4. No changes required.
  * Update debhelper dependency to 9 for multiarch.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 29 Jan 2013 16:18:53 +0000

adios (1.3-10) unstable; urgency=low

  * Patch from Simon Ruderich for hardening flags. Closes: #663925, #663978. 

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 21 Mar 2012 10:20:55 +0000

adios (1.3-9) unstable; urgency=low

  * Move to Standards-Version: 3.9.3. No changes required.
  * Fixes to build with hardening flags.  
  * Patch from Pino Toscano to fix FTBFS on Hurd. Closes: #660562.
  * Fix netcdf linking. Closes: #662995 
  * Change to Multiarch: foreign now we ship arch-dependent binaries
    in libadios-dev. 

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 09 Mar 2012 17:34:24 +0000

adios (1.3-8) unstable; urgency=low

  * Add -Wl,--as-needed to remove unnecessary dependencies.
  * Mult-Arch support. 
    - No longer ship adios_config.flags. Instead recommend users use  
      pkg-config, which Does The Right Thing in multi-arch.
    - adios_

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 14 Sep 2011 15:43:29 +0100

adios (1.3-7) unstable; urgency=low

  * Rename gpp to adiosxml2h. (Will be renamed by upstream in next release)
    Closes: #639257. 
  * Move DH_COMPAT=8 

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 25 Aug 2011 18:17:09 +0100

adios (1.3-6) unstable; urgency=low

  * LAM uses mpicc.lam, not mpicc.  

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 16 Aug 2011 17:18:36 +0100

adios (1.3-5) unstable; urgency=low

  * MPI_IN_PLACE patch is needed for adiso_mpi_lustre.c too, even though
   Lustre is not used in LAM environment. Sigh.  

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 16 Aug 2011 09:57:26 +0100

adios (1.3-4) unstable; urgency=low

  * Patch to stop compilation with dummy MPI if real parallel HDF5 is used.
    Closes: #637893. 
  * make use of MPI_IN_PLACE conditional on MPI_IN_PLACE existing,
    as its not present in LAM MPI. Also, conditionally use mpicc.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 15 Aug 2011 17:57:34 +0100

adios (1.3-3) unstable; urgency=low

  * Make lustre support conditional to intel,amd64, powerpc archs. 

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 15 Aug 2011 16:56:23 +0100

adios (1.3-2) unstable; urgency=low

  * Set CC=mpicc to ensure new C code gets built with MPI. 
    Closes: #637714. 
  * Add lustre support. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 14 Aug 2011 16:12:14 +0100

adios (1.3-1) unstable; urgency=low

  * New upstream release.
  * Ignore -little flag in configure to support SH4. 
    Thanks to Nobuhiro Iwamatsu; Closes: #635908. 

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 09 Aug 2011 20:32:58 +0100

adios (1.2.1-2) unstable; urgency=low

  * Use MPIFC=mpif77 during build, to work on LAM-based MPI systems. 

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 10 Jul 2011 00:33:10 +0100

adios (1.2.1-1) unstable; urgency=low

  * Initial release. (Closes: #624371).

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 27 Apr 2011 19:16:12 +0100
